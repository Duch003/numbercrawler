# NumberCrawler
## Description
This is simple web crawler. Its task is to search for phone numbers on the [Og�aszamy24](http://www.oglaszamy24.pl/) website. This scipt is intended to help any person to check if her/his phone number is used somewhere without her/his knowledge.

## Dependencies
* [Python3](https://www.python.org/),
* [Powershell](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell?view=powershell-7.2) or [Bash](https://git-scm.com/downloads) console - to run the program (Powershell script provided),
* [PyCharm](https://www.jetbrains.com/pycharm/download/#section=windows) or any kind of IDE - only if you want to modify the code

## Used libraries
All used libraries can be found in requirements.txt file

## Installation
1. Ensure Python3 is installed and added to the system PATH,
2. Clone repository,
3. Open repository root location using system console,
4. Execute `pip3 install /r requirements.txt` - this command will install all dependencies,
5. Update config.json according to the instruction,
6. Execute `python main.py` - this command will start processing the website,
7. If you want to stop the execution before its finish, simply click **Ctrl + C** while focused on the console.

## Configuration
Certain properties can be set in `config.json` file:

1. **[Required]** *pageToSearch* - url path leading to the page with listed user advertisements,
   
2. **[Required]** *domain* - website domain, leave this parameter as it is,
   
3. **[Required]** *delayInSeconds* - a `float` number, this will delay each request sent to the page for the given time in seconds,
   
4. **[Required]** *startingPage* - in case of pagination on the page, this parameter will provide first page number, required to browse all pages. Leave this parameter as it is,
   
5. **[Optional]** *numberToSearch* - if provided, the script will print out all urls with a phone number containing provided string; if empty - all pages with phone numbers will be printed.

**Examples:**
```
{
   //This setting will print only advertisements with phone number 123123123, each request delayed by 0.5 second
   "pageToSearch": "ogloszenia/wypoczynek/pokoje-i-kwatery-prywatne/",
   "domain": "http://www.oglaszamy24.pl/",
   "delayInSeconds": 0.5,
   "startingPage": 1,
   "numberToSearch": "123123123"
}
```
```
{
   //This setting will print all advertisements with phone numbers, each request delayed by 1 second
   "pageToSearch": "ogloszenia/wypoczynek/pokoje-i-kwatery-prywatne/",
   "domain": "http://www.oglaszamy24.pl/",
   "delayInSeconds": 1,
   "startingPage": 1,
   "numberToSearch": ""
}
```

## Screenshots:
![Example result](./img/ResultExample.PNG "Sample")
![Source1](./img/Page1.png "Source 1")
![Source2](./img/Page2.png "Source 2")