import bs4
import requests
from datetime import datetime
import time
import json

from requests import Response


def log(text: str):
    """
    Simple method to print messages in the console
    :param text: message to be printed in the console
    """
    msg = f'[{datetime.today().strftime("%d/%m/%Y %H:%M:%S")}] {text}'
    print(msg)


def log_response_error(response: Response):
    """
    A special case of log to be logged
    :param response: httpResponse object to be logged in the console
    """
    log(f'Response does not indicate success, please see the message: [{response.status_code}] [{response.url}] '
        f'{response.text}')


def get_phone_number_if_any(html: bs4.BeautifulSoup):
    """
    Retrieves the element containing telephone number from the page
    :param html: html to be searched
    :return: string containing telephone number, None if not found
    """
    phone_number = html.select('.c_phone span')
    if len(phone_number) > 0:
        return phone_number[0].getText()
    return None


def get_page(url: str):
    """
    Sends requests for the given url
    :param url: a valid url
    :return: Response object
    """
    response = requests.get(url)
    response.raise_for_status()
    return response


def get_html(response: Response):
    """
    Transforms text from http response to BeautifulSoup object
    :param response: http response
    :return: BeautifulSoup with browsable html tree
    """
    return bs4.BeautifulSoup(response.text, features="html.parser")


def get_links(html: bs4.BeautifulSoup):
    """
    Gets all links to the advertisements from the html tree
    :param html: a html tree to be browsed
    :return: ResultSet containing all found anchor tags leading to advertisements
    """
    return html.select('a.o_title')


def get_next_page_element(next_page_number: int, html: bs4.BeautifulSoup):
    """
    Searches for the next page url in the given html tree
    :param next_page_number: expected number to be found
    :param html: a html tree to be browsed
    :return: a list of all anchor tags having the exact expected number in the text property
    """
    return html.find(lambda tag: tag.name == 'a' and str(next_page_number) == tag.text.strip())


def remove_white_spaces(text: str):
    """
    Replaces all whitespaces in the given string
    :param text: string to transform
    :return: cleared string
    """
    return text.replace(' ', '')


def analyze_page(url: str, page_number: int):
    """
    Main searching function - browses given page looking for advertisements, then phone numbers in them
    :param url: 'www.oglaszamy24.pl' valid url
    :param page_number: current page number
    """

    log(f'Searching: {url}')

    # Get the page to browse
    response = get_page(url)
    if response.status_code != 200:
        # Probably error, at this moment I do not care about 200-ish responses beside 200
        log_response_error(response)
        log('Going on...')
        return
    log('Page downloaded successfully')

    # Transform page text to browsable html tree
    page_html = get_html(response)

    # Get all user advertisements
    links = get_links(page_html)

    # Search each advertisement
    for link in links:
        # I am not quite sure if this delay is necessary, in my mind I just wanted to fool a bit the website anti-bot
        # security (if any) simulating a little delay, but probably it won't be that easy.
        # Maybe I will implement some kind of time randomizer in the future, but at this moment works fine.
        time.sleep(float(config['delayInSeconds']))

        # Read anchor (advertisement) url
        ad_url = link.get('href')

        # Request the advertisement content
        advertisement_page_response = get_page(ad_url)
        if advertisement_page_response.status_code != 200:
            # Probably error, at this moment I do not care about 200-ish responses beside 200
            log_response_error(advertisement_page_response)
            log('Going on...')
            continue

        # Convert content to browsable html tree
        ad_html = get_html(advertisement_page_response)

        # Search for phone number
        phone_number = get_phone_number_if_any(ad_html)
        if phone_number is not None and config['numberToSearch'] in remove_white_spaces(phone_number):
            log(f'[{ad_url}] {phone_number}')

    # Check if there are more pages to browse
    next_page_element = get_next_page_element(page_number + 1, page_html)
    if next_page_element is not None:
        next_page_url = config['domain'] + next_page_element.get('href')
        log(f'Next page detected: {next_page_url} Going in...')
        log('')
        analyze_page(next_page_url, page_number + 1)


def get_config():
    """
    Loads and returns config as dictionary
    :return: config as dictionary object
    """
    with open('config.json', 'r') as file:
        content = file.read()
    return json.loads(content)


if __name__ == '__main__':
    try:
        # Config acts as global object here, no need to pass it every time
        config = get_config()
        analyze_page(config['domain'] + config['pageToSearch'], int(config['startingPage']))
    except Exception as e:
        # I know that leaving this code without try-except block will do the same thing, but I am leaving it as 
        # boilerplate for future modifications
        log('An error occured while executing the script, please see the message below.')
        raise
    log('Process finished successfully')
